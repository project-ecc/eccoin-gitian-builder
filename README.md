# Building Eccoin with Gitian

Gitian is used to do a deterministic build of a package inside a VM.

Read about the project goals at the [project home page](https://gitian.org/).

These instructions are for building eccoin using Gitian. For full instructions and details about the workings of Gitian please see the upstream repository [here](https://github.com/devrandom/gitian-builder).

## Deterministic build inside a VM

This performs a build inside a VM, with deterministic inputs and outputs.  If the build script takes care of all sources of non-determinism (mostly caused by timestamps), the result will always be the same.  This allows multiple independent verifiers to sign a binary with the assurance that it really came from the source they reviewed.

## Prerequisites:

### Ubuntu (18.04+ required)

To setup the builder run the setup.sh script. It does need sudo to install dependencies and add user to the docker group.

## Building Eccoin

To build eccoin run the `eccoin_builder.sh` script
```
eccoin_builder.sh
```

By default this script will build the the last release (0.3.3.0), you can change the branch that should be built by editing the `TAG` variable in eccoin_builder.sh script. `TAG` can accept a gitlab tag, branch name, or commit hash.

By default this script will build Linux, Mac, Windows, and ARM. To toggle building for an operating system set the `BUILD` variable for that operating system = 0 to disable or = 1 to enable.
